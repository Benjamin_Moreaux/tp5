﻿using System;
using Mesozoic;
using System.Collections.Generic;

namespace mesozoicconsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello from Mesozoic!");
            Dinosaur louis, nessie;

            louis = new Dinosaur("Louis", "Stegausaurus", 12);
            nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            philipe = new Dinosaur("philipe", "Brachiosaurus", 13);
            louise = new Dinosaur("louise", "Triceratops", 15);

            List<Dinosaur> dinosaurs = new List<Dinosaur>();

            dinosaurs.Add(louis);
            dinosaurs.Add(nessie);
            dinosaurs.Add(philipe);
            dinosaurs.Add(louise);

            Console.WriteLine(dinosaurs.Count);
            foreach (Dinosaur dino in dinosaurs)
            {
                Console.WriteLine(dino.sayHello);
            }

            dinosaurs.RemoveAt(1);

            Console.WriteLine(dinosaurs.Count);
            foreach(Dinosaur dino in dinosaurs)
            {
                Console.WriteLine(dino.sayHello);
            }

            dinosaurs.Remove(louis);

            Console.WriteLine(dinosaurs.Count);

            foreach (Dinosaur dino in dinosaurs)
            {
                Console.WriteLine(dino.sayHello);
            }
            Console.ReadKey;
        }
    }
}
